#include <QApplication>
#include <QPalette>
#include <QDebug>
#include "contactmodel.h"
#include "contactdata.h"

ContactModel::ContactModel(QObject *parent)
    : QAbstractListModel(parent)
{
    beginResetModel();
    count = 0;
    endResetModel();
}

int ContactModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return count;
}

QVariant ContactModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= ContactData::instance()->getAllContact().size() || index.row() < 0)
        return QVariant();

    ContactInfo *contactInfo = ContactData::instance()->getAllContact().value(index.row());
    if (role == Qt::DisplayRole) {
        return contactInfo->name;
    } else if (role == Qt::BackgroundRole) {
        int batch = (index.row() / 10) % 2;
        if (batch == 0)
            return qApp->palette().base();
        else
            return qApp->palette().alternateBase();
    } else if (role == Qt::UserRole + 1) { // avatar color
        QVariant tmp;
        //qDebug() << contactInfo->avatarColor.name();
        tmp.setValue(contactInfo->avatarColor);
        return tmp;
    } else if (role == Qt::UserRole + 2) { // sign
        //qDebug() << contactInfo->lastMsgContent;
        return contactInfo->lastMsgContent;
    } else if (role == Qt::UserRole + 3) { // unread num
        return contactInfo->unreadCount;
    }

    return QVariant();
}

bool ContactModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (index.row() >= ContactData::instance()->getAllContact().size() || index.row() < 0)
        return false;

    ContactInfo *contactInfo = ContactData::instance()->getAllContact().value(index.row());
    if (role == Qt::DisplayRole) {
        contactInfo->name = value.toString();
        return true;
    } else if (role == Qt::UserRole + 1) {
        contactInfo->avatarColor = value.value<QColor>();
        return true;
    } else if (role == Qt::UserRole + 2) {
        contactInfo->lastMsgContent = value.toString();
        return true;
    } else if (role == Qt::UserRole + 3) { // unread num
        contactInfo->unreadCount = value.toInt();
        return true;
    }
    return false;
}

Qt::ItemFlags ContactModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return Qt::ItemIsSelectable | Qt::ItemNeverHasChildren | Qt::ItemIsEnabled;
}

bool ContactModel::canFetchMore(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    if (count < ContactData::instance()->getAllContact().size()) {
        return true;
    }
    return false;
}

void ContactModel::fetchMore(const QModelIndex &parent)
{
    Q_UNUSED(parent)
    int remainder = ContactData::instance()->getAllContact().size() - count;
    int itemsToFetch = qMin(10, remainder);

    beginInsertRows(QModelIndex(), count, count+itemsToFetch-1);

    count += itemsToFetch;

    endInsertRows();
}
