#include <QDebug>
#include "widget.h"
#include "ui_widget.h"
#include "contactdata.h"
#include "contactdelegate.h"
#include "contactmodel.h"
#include "recentcontactmodel.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    contactModel(new ContactModel(this)),
    recentContactModel(new RecentContactModel(this))
{
    ui->setupUi(this);

    ui->listView_all->setItemDelegate(new ContactDelegate(ui->listView_all));
    ui->listView_all->setModel(contactModel);

    //ui->listView_recent->setItemDelegate(new ContactDelegate(ui->listView_recent));
    ui->listView_recent->setModel(recentContactModel);
}

Widget::~Widget()
{
    delete ui;
}
