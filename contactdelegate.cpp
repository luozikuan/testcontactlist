#include <QPainter>
#include <QDebug>
#include <QMouseEvent>
#include <QVariant>
#include <QMenu>
#include <QFontMetricsF>
#include "contactdelegate.h"

ContactDelegate::ContactDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{
}

void ContactDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->setRenderHint(QPainter::Antialiasing);
    QRectF itemRect = QRectF(option.rect);

    if (option.state & QStyle::State_Selected) {
        painter->fillRect(itemRect, Qt::blue);
        qDebug() << index.data() << "selected";
    } else if (option.state & QStyle::State_MouseOver) {
        painter->fillRect(itemRect, Qt::gray);
    }

    QRectF avatarRect = QRectF(itemRect.topLeft() + QPointF(3, 3), QSizeF(50, 50));
    QColor color = index.data(Qt::UserRole + 1).value<QColor>();
    painter->fillRect(avatarRect, color);

    int unread = index.data(Qt::UserRole + 3).toInt();
    if (unread != 0) {
        QString num;
        if (unread > 99)
            num = QString("99+");
        else
            num = QString::number(unread);
        QFontMetricsF metricF(painter->font());
        QSizeF unreadTextSize = metricF.size(Qt::TextSingleLine, num);
        qreal minHeight = 18.0;
        if (unreadTextSize.height() < minHeight) unreadTextSize.setHeight(minHeight);
        if (unreadTextSize.width() < minHeight) unreadTextSize.setWidth(minHeight);
        if (unreadTextSize.width() > unreadTextSize.height()) unreadTextSize.setWidth(unreadTextSize.width() + 6);

        QRectF unreadRect = avatarRect.adjusted(avatarRect.width() - unreadTextSize.width(), 0, 0, unreadTextSize.height() - avatarRect.height());
        QPainterPath shadowPath;
        shadowPath.addRoundedRect(unreadRect.adjusted(1,1,1,1), unreadTextSize.height()/2, unreadTextSize.height()/2);
        painter->fillPath(shadowPath, QColor(0,0,0,200));
        QPainterPath path;
        path.addRoundedRect(unreadRect, unreadTextSize.height()/2, unreadTextSize.height()/2);
        painter->fillPath(path, QColor(Qt::red).dark());

        painter->setPen(Qt::white);
        painter->drawText(unreadRect, Qt::AlignCenter, num);
        painter->setPen(Qt::black);
    }

    QRectF nicknameRect = itemRect.adjusted(56, 3, 0, -56/2 - 3);
    QString nickname = index.data(Qt::DisplayRole).toString();
    painter->drawText(nicknameRect, Qt::AlignLeft | Qt::AlignVCenter, nickname);

    QRectF signRect = itemRect.adjusted(56, 56/2 - 3, 0, - 3);
    QString sign = index.data(Qt::UserRole + 2).toString();
    painter->drawText(signRect, Qt::AlignLeft | Qt::AlignVCenter, sign);
}

QSize ContactDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(100, 56);
}

bool ContactDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{
    if (event->type() == QEvent::MouseButtonPress) {
        QMouseEvent *mouseEvent = dynamic_cast<QMouseEvent*>(event);
        if (mouseEvent) {
            if (mouseEvent->buttons() & Qt::LeftButton) {
                QRect avatarRect = QRect(option.rect.topLeft() + QPoint(3, 3), QSize(50, 50));
                if (avatarRect.contains(mouseEvent->pos())) {
                    qDebug() << "left clicked on avatar of item" << index.data();
                    model->setData(index, index.data().toString() + tr("changed"), Qt::DisplayRole);
                } else {
                    qDebug() << "left clicked on item" << index.data();
                    model->setData(index, 0, Qt::UserRole + 3);
                }
                event->accept();
            } else if (mouseEvent->buttons() & Qt::RightButton) {
                qDebug() << "right clicked on item" << index.data();
                QMenu menu;
                menu.addAction(tr("delete this contact"));
                menu.addAction(tr("delete all contact"));
                menu.exec(QCursor::pos());
                event->accept();
                return true;
            }
        }
    } else {
        event->ignore();
        return false;
    }
}
